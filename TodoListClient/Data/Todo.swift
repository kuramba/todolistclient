//
//  Todo.swift
//  TodoListClient
//
//  Created by Oren Idan on 09/07/2017.
//  Copyright © 2017 Oren Idan. All rights reserved.
//

struct Todo: Codable {
    let id: Int
    var title: String
    var completed: Bool = false
}
