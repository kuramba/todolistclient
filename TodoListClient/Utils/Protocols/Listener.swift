//
//  ListenerValue.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

class Listener<T> {
    typealias Listener = (T) -> Void
    private var listener: Listener?

    func bind(_ listener: Listener?) {
        self.listener = listener
    }

    func bindAndFire(_ listener: Listener?) {
        self.listener = listener
        listener?(value)
    }

    var value: T {
        didSet {
            listener?(value)
        }
    }

    init(_ v: T) {
        value = v
    }
}
