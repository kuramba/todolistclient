//
//  TodoApiAddNew.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

protocol TodoApiAddNewDelegate: class {
    func serverCreated(todo: Todo)
}

class TodoApiAddNew {
    private weak var delegate: TodoApiAddNewDelegate?

    func addNew(title: String, delegate: TodoApiAddNewDelegate) {
        self.delegate = delegate
        NetworkManager().makeRequest(api: .addNew, httpMethod: .post, parameters: ["title": title],
                                     completionCallBack: self)
    }
}

extension TodoApiAddNew: NetworkManagerResponse {
    func successfullResponse(responseObjects: Todo?) {
        if let responseObjects = responseObjects {
            delegate?.serverCreated(todo: responseObjects)
        }
    }
}
