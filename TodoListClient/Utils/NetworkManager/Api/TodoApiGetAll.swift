//
//  TodoApiGetAll.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

protocol TodoApiGetAllDelegate: class {
    func serverGotAll(todos: [Todo])
}

class TodoApiGetAll {
    private weak var delegate: TodoApiGetAllDelegate?

    func getAll(delegate: TodoApiGetAllDelegate) {
        self.delegate = delegate
        NetworkManager().makeRequest(api: .getAll, httpMethod: .get, completionCallBack: self)
    }
}

extension TodoApiGetAll: NetworkManagerResponse {
    typealias Todos = [Todo]

    func successfullResponse(responseObjects: [Todo]?) {
        if let responseObjects = responseObjects {
            delegate?.serverGotAll(todos: responseObjects)
        }
    }
}
