//
//  TodoApiDelete.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

protocol TodoApiDeleteDelegate: class {
    func serverDeleted(todoId: Int)
}
class TodoApiDelete {
    private weak var delegate: TodoApiDeleteDelegate?
    var todoId: Int = 0

    func delete(todo: Todo, delegate: TodoApiDeleteDelegate?) {
        self.delegate = delegate
        self.todoId = todo.id
        NetworkManager().makeRequest(api: Api.delete(todo: todo), httpMethod: .delete, completionCallBack: self)
    }
}

extension TodoApiDelete: NetworkManagerResponse {
    typealias ResponseObject = String

    func successfullResponse(responseObjects: String?) {
        delegate?.serverDeleted(todoId: todoId)
    }
}
