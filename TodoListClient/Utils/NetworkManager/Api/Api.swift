//
//  Api.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

import Foundation
import Alamofire

enum Api {

    struct Constants {
        static let baseUrl = "http://ec2-52-32-105-2.us-west-2.compute.amazonaws.com:8080/"
    }

    case getAll
    case addNew
    case update(todo: Todo)
    case delete(todo: Todo)

    func url() -> String {
        switch self {
        case .getAll: return "all"
        case .addNew: return "new"
        case .update(let todo): return "update/\(todo.id)"
        case .delete(let todo): return "delete/\(todo.id)"
        }
    }
}

extension NetworkManager {

    /// NetworkManager Api Interface
    func makeRequest<T: NetworkManagerResponse> (baseUrl: String? = Api.Constants.baseUrl, api: Api,
                                                 httpMethod: HTTPMethod, parameters: [String: Any]? = nil,
                                                 completionCallBack: T) {

        guard let url = URL(string: (baseUrl ?? "") + api.url()) else { return }
        makeRequest(url: url, httpMethod: httpMethod, parameters: parameters, completionCallBack: completionCallBack)

    }
}
