//
//  TodoApiUpdate.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

protocol TodoApiUpdateDelegate: class {
    func serverUpdated(todo: Todo)
}
class TodoApiUpdate {
    private weak var delegate: TodoApiUpdateDelegate?

    func update(todo: Todo, delegate: TodoApiUpdateDelegate?) {
        self.delegate = delegate
        NetworkManager().makeRequest(api: Api.update(todo: todo), httpMethod: .put,
                                     parameters: ["completed": todo.completed],
                                     completionCallBack: self)
    }
}

extension TodoApiUpdate: NetworkManagerResponse {
    typealias ResponseObject = Todo

    func successfullResponse(responseObjects: Todo?) {
        if let responseObjects = responseObjects {
            delegate?.serverUpdated(todo: responseObjects)
        }
    }
}
