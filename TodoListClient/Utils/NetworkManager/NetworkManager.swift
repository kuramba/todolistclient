//
//  NetworkManager.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkManagerResponse {
    associatedtype ResponseObject: Decodable

    /// successfull response
    func successfullResponse(responseObjects: ResponseObject?)
    /// Error response
    func errorResponse()
    /// Mechanizm method
    func getType() -> ResponseObject.Type
}
extension NetworkManagerResponse {
    func getType() -> ResponseObject.Type {
        return ResponseObject.self
    }

    func errorResponse() {
        // maybe show some error message on some calls ?
    }
}

class NetworkManager {

    func makeRequest<T: NetworkManagerResponse> (url: URL, httpMethod: HTTPMethod,
                                                 parameters: [String: Any]? = nil,
                                                 completionCallBack: T) {

        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue

        if let parameters = parameters {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                print("Error parsing: \(error)")
            }
        }

        Alamofire.request(request).responseData { response in
                guard response.result.isSuccess,
                    let data = response.data else {
                        completionCallBack.errorResponse()
                        print("Error fetching: \(String(describing: response.result.error))")
                        return
                }

                if data.isEmpty,
                    (response.response?.statusCode ?? 0) >= 200,
                    (response.response?.statusCode ?? 0) < 300 {
                    // seems that server can return empty response as good response
                    completionCallBack.successfullResponse(responseObjects: nil)
                    return
                }

                do {
                    let decoder = JSONDecoder()
                    let parsedData = try decoder.decode(completionCallBack.getType(), from: data)
                    completionCallBack.successfullResponse(responseObjects: parsedData)
                    return
                } catch {
                    print("Error parsing: \(error)")
                }

                completionCallBack.errorResponse()
        }
    }
}
