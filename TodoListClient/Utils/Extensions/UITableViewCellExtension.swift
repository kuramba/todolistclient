//
//  UITableViewCellExtension.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static func reuseId() -> String {
        return String(describing: self)
    }
}
