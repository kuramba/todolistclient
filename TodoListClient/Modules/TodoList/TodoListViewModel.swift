//
//  TodoListViewModel.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

import UIKit

// MARK: - TodoListViewModelInterface
protocol TodoListViewModelInterface {

    // network calls

    func getAll()
    func update(todo: Todo)
    func addNew(title: String)
    func delete(todo: Todo)

    // local data

    func getTodo(at index: Int) -> Todo?
    func getTodoCount() -> Int

    // Pure MVVM will require us to move alert to VC, with tf on the go, it will require to redfine a lot
    // another way is to put somewhere more reusable and get VC from UIWindow
    // will do one of those if we keep the project
    func showAlertToCreateNewTodoObject(at viewController: UIViewController)
}

// MARK: - TodoListViewModel
class TodoListViewModel: TodoListViewModelInterface {

    var todoList: Listener<[Todo]>

    required init() {
        todoList = Listener([])
    }

    // MARK: - Screen actions

    func getAll() {
        TodoApiGetAll().getAll(delegate: self)
    }

    func update(todo: Todo) {
        updateLocalDataSource(todo: todo)
        TodoApiUpdate().update(todo: todo, delegate: self)
    }

    func addNew(title: String) {
        TodoApiAddNew().addNew(title: title, delegate: self)
    }

    func delete(todo: Todo) {
        TodoApiDelete().delete(todo: todo, delegate: self)
    }

    func getTodo(at index: Int) -> Todo? {
        guard todoList.value.count > index else { return nil }
        return todoList.value[index]
    }

    func getTodoCount() -> Int {
        return todoList.value.count
    }

    // Pure MVVM will require us to move alert to VC, with tf on the go, it will require to redfine a lot
    // another way is to put somewhere more reusable and get VC from UIWindow
    // will do one of those if we keep the project
    func showAlertToCreateNewTodoObject(at viewController: UIViewController) {

        let alertController = UIAlertController(title: "Add New Item", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField: UITextField!) -> Void in
            textField.placeholder = "Enter Task"
        }
        let saveAction = UIAlertAction(title: "Add", style: .default) { [weak self] (_) in
            let textField = alertController.textFields![0]
            if let text = textField.text {
                self?.addNew(title: text)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {_ in})

        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        viewController.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Local datasource update
extension TodoListViewModel {
    @discardableResult
    private func updateLocalDataSource(todo: Todo) -> Int? {
        if let updateIndex = todoList.value.firstIndex(where: { (todoCheck) -> Bool in
            return todoCheck.id == todo.id
        }) {
            todoList.value[updateIndex] = todo
            return updateIndex
        }
        return nil
    }

    @discardableResult
    private func deleteFromLocalDataSource(todoId: Int) -> Int? {
        if let deleteIndex = todoList.value.firstIndex(where: { (todoCheck) -> Bool in
            return todoCheck.id == todoId
        }) {
            todoList.value.remove(at: deleteIndex)
            return deleteIndex
        }
        return nil
    }
}

// MARK: - TodoApiGetAllDelegate
extension TodoListViewModel: TodoApiGetAllDelegate {
    func serverGotAll(todos: [Todo]) {
        todoList.value = todos
    }
}

// MARK: - TodoApiAddNewDelegate
extension TodoListViewModel: TodoApiAddNewDelegate {
    func serverCreated(todo: Todo) {
        todoList.value.append(todo)
    }
}

// MARK: - TodoApiUpdateDelegate
extension TodoListViewModel: TodoApiUpdateDelegate {
    func serverUpdated(todo: Todo) {
        updateLocalDataSource(todo: todo)
    }
}

// MARK: - TodoApiDeleteDelegate
extension TodoListViewModel: TodoApiDeleteDelegate {
    func serverDeleted(todoId: Int) {
        deleteFromLocalDataSource(todoId: todoId)
    }
}
