//
//  TodoListDisplayTableViewCell.swift
//  TodoListClient
//
//  Created by Oren Idan on 09/07/2017.
//  Copyright © 2017 Oren Idan. All rights reserved.
//

import UIKit

protocol TodoListDisplayTableViewCellDelegate: class {
    func todoTapped(at cell: TodoListDisplayTableViewCell)
}

class TodoListDisplayTableViewCell: UITableViewCell {

    private struct Constants {
        static let actionButtonCompleteText = "Complete"
        static let actionButtonUncompleteText = "Uncomplete"
    }

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var actionButton: UIButton!

    @IBAction private func action(_ sender: UIButton) {
        delegate?.todoTapped(at: self)
    }

    private weak var delegate: TodoListDisplayTableViewCellDelegate?

    func configure(todo: Todo, delegate: TodoListDisplayTableViewCellDelegate?) {
        self.delegate = delegate
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: todo.title)
        let actionButtonText: String
        if todo.completed {
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle,
                                         value: 2, range: NSRange(location: 0, length: attributeString.length))
            actionButtonText = Constants.actionButtonUncompleteText
        } else {
            actionButtonText = Constants.actionButtonCompleteText
        }
        actionButton.setTitle(actionButtonText, for: .normal)
        titleLabel.attributedText = attributeString
    }
}
