//
//  TodoListViewController.swift
//  TodoListClient
//
//  Created by Alexei Ilin on 16/02/2019.
//  Copyright © 2019 Oren Idan. All rights reserved.
//

import UIKit

class TodoListViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet private weak var table: UITableView!

    // MARK: - Property

    private var viewModel: TodoListViewModelInterface! {
        didSet { viewModel.getAll() }
    }


    // MARK: - LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
    }

    // MARK: - Private

    private func setupView() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createButtonClicked))

        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = UITableView.automaticDimension
    }

    private func setupViewModel() {
        let viewModel = TodoListViewModel()
        viewModel.todoList.bind { [weak self] (_) in
            DispatchQueue.main.async {
                self?.table.reloadData()
            }
        }
        self.viewModel = viewModel
    }

    @objc private func createButtonClicked() {
        viewModel.showAlertToCreateNewTodoObject(at: self)
    }
}

// MARK: - UITableViewDelegate
extension TodoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getTodoCount()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let todo = viewModel.getTodo(at: indexPath.row),
            let cell = tableView.dequeueReusableCell(withIdentifier: TodoListDisplayTableViewCell.reuseId(),
                                                     for: indexPath) as? TodoListDisplayTableViewCell else { return UITableViewCell() }
        cell.configure(todo: todo, delegate: self)

        return cell
    }
}

// MARK: - UITableViewDataSource
extension TodoListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let todo = viewModel.getTodo(at: indexPath.row) {
                viewModel.delete(todo: todo)
            }
        }
    }
}

// MARK: - TodoTableViewCellDelegate
extension TodoListViewController: TodoListDisplayTableViewCellDelegate {
    func todoTapped(at cell: TodoListDisplayTableViewCell) {
        guard let indexPath = table.indexPath(for: cell) else { return }
        if var todo = viewModel.getTodo(at: indexPath.row) {
            todo.completed = !todo.completed
            viewModel.update(todo: todo)
        }
    }
}
